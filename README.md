# OpenML dataset: Sales_DataSet_of_SuperMarket

https://www.openml.org/d/43605

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The Story
This data set was part of my online course material for Data Analysis using Python over at Udemy.
The Contents
The dataset is very useful for beginners and novice number crunchers looking to run queries in a relatable and easy-to-understand dataset. It includes the data about shoppers of a supermarket chain having different locations and the total of their purchases.
Acknowledgements
This dataset was organised with the help of Ashutosh Pawar at Udemy.
Inspiration
I want this database to be for beginners venturing into Data Science, a dataset so relatable and commonplace. Ultimately, driving home the point that Data Science itself is for solving real life problems.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43605) of an [OpenML dataset](https://www.openml.org/d/43605). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43605/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43605/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43605/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

